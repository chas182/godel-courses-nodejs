const {Post} = require('../models');
const PostService = require('../services/postService');
const postService = new PostService(Post);

class PostController {
  async getPosts(req, res) {
  	let data;
  	try {
  		data = await postService.getAll();
  	} catch (err) {
  		res.status(500).send({ message: err.message });
  	}
  	res.send(data);
  }

  async getPostById(req, res) {
  	let data;
  	try {
  		data = await postService.getById(Number(req.params.id));
  	} catch (err) {
  		res.status(err.status).send({ message: err.message });
  	}
  	res.send(data);
  }

  async addPost(req, res) {
    let data;
    try {
      data = await postService.insert(req.body);
    } catch (err) {
      res.status(500).send({ message: err.message });
    }
    res.status(201).send(data);
  }

  async deletePostById(req, res) {
    let number;
    try {
      number = await postService.deleteById(Number(req.params.id));
    } catch (err) {
      res.status(err.status).send({ message: err.message });
    }
    res.sendStatus(number > 0 ? 200 : 404);
  }

  async updatePost(req, res) {
  	let number;
  	try {
  		number = await postService.update(req.body);
  	} catch (err) {
  		res.status(500).send({ message: err.message });
  	}
  	res.sendStatus(number > 0 ? 200 : 404);
  }
}

module.exports = new PostController();
