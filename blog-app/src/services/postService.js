module.exports = class PostService {
	constructor(Post) {
    this.Post = Post;
  }

	async getAll() {
		const posts = await this.Post.findAll();
		return posts;
	}

	async getById(id) {
		const post = await this.Post.findByPk(id);
		return post;
	}

	async insert(data) {
		const post = await this.Post.create(data);
		return post;
	}

	async update(data) {
		const number = await this.Post.update(data, { where: {id: data.id } });
		return number;
	}

	async deleteById(id) {
		const number = await this.Post.destroy({ where: {id: id } });
		return number;
	}
}
