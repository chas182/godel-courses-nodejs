const express = require('express');
const router = express.Router();

const postController = require('../controllers/postController');

router.get('/', postController.getPosts);
router.get('/:id', postController.getPostById);
router.post('/', postController.addPost);
router.delete('/:id', postController.deletePostById);
router.put('/', postController.updatePost);

module.exports = router;
