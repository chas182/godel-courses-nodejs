const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

const server = require('../src/index');
const {Post} = require('../src/models');

chai.use(chaiHttp);

describe('POSTS', () => {

  describe('GET /api/v0.1/posts', () => {

    beforeEach(async () => {
      await Post.destroy({
        where: {},
        truncate: true
      });
    });

    it('should return empty array', (done) => {
      chai.request(server)
      .get('/api/v0.1/posts')
      .end((err, res) =>{
        const { status, body } = res;
        expect(status).to.be.eq(200);
        expect(body).to.be.empty;
        done();
      });
    });

    it('should return one item', async () => {
      const post = {
        title: 'Title',
        description: 'Description',
        author: 1
      };

      await Post.create(post);

      const result = await chai.request(server)
      .get('/api/v0.1/posts');

      const { status, body } = result;

      expect(status).to.be.eq(200);
      expect(body).to.have.lengthOf(1);
      expect(body[0]).to.include(post);

    });

    it('should return one item by id', async () => {
      const post = {
        title: 'Title',
        description: 'Description',
        author: 1
      };

      const { id } = await Post.create(post);
      const result = await chai.request(server)
      .get('/api/v0.1/posts/' + id);

      const { status, body } = result;

      expect(status).to.be.eq(200);
      expect(body).to.include(post);
    });

  });

  describe('POST /api/v0.1/posts', () => {

    beforeEach(async () => {
      await Post.destroy({
        where: {},
        truncate: true
      });
    });

    it('should save item', async () => {
      const post = {
        title: 'Title Saved',
        description: 'Description Saved',
        author: 1
      };

      const result = await chai.request(server)
      .post('/api/v0.1/posts')
      .set('content-type', 'application/json')
      .send(post);

      const { status, body } = result;
      expect(status).to.be.eq(201);
      expect(body).to.include(post);
      expect(body).to.have.own.property('id');
      expect(body).to.have.own.property('createdAt');
      expect(body).to.have.own.property('updatedAt');
    });

  });

  describe('PUT /api/v0.1/posts', () => {

    beforeEach(async () => {
      await Post.destroy({
        where: {},
        truncate: true
      });
    });

    it('should update item', async () => {
      const post = await Post.create({
        title: 'Title Saved',
        description: 'Description Saved',
        author: 1
      });
      const {id} = post;
      const result = await chai.request(server)
      .put('/api/v0.1/posts')
      .set('content-type', 'application/json')
      .send({
        id: id,
        title: 'Title Updated',
        description: 'Description Updated',
        author: 1
      });

      const { status } = result;
      expect(status).to.be.eq(200);
      const updatedPost = await Post.findByPk(id);
      expect(updatedPost.title).to.be.eq('Title Updated');
      expect(updatedPost.description).to.be.eq('Description Updated');
    });

    it('should not update item when id is incorrect', async () => {
      const post = await Post.create({
        title: 'Title Saved',
        description: 'Description Saved',
        author: 1
      });
      const {id} = post;
      const result = await chai.request(server)
      .put('/api/v0.1/posts')
      .set('content-type', 'application/json')
      .send({
        id: id + 200,
        title: 'Title Updated',
        description: 'Description Updated',
        author: 1
      });

      const { status } = result;
      expect(status).to.be.eq(404);
    });

  });

  describe('DELETE /api/v0.1/posts', () => {

    beforeEach(async () => {
      await Post.destroy({
        where: {},
        truncate: true
      });
    });

    it('should delete item', async () => {
      const post = await Post.create({
        title: 'Title',
        description: 'Description',
        author: 1
      });

      const { id } = post;
      const result = await chai.request(server)
      .delete('/api/v0.1/posts/' + id);

      const { status } = result;
      expect(status).to.be.eq(200);
    });

    it('should not delete item when id is incorrect', async () => {
      const post = await Post.create({
        title: 'Title',
        description: 'Description',
        author: 1
      });

      const { id } = post;
      const result = await chai.request(server)
      .delete('/api/v0.1/posts/' + (id + 200));

      const { status } = result;
      expect(status).to.be.eq(404);
    });
  });
});
